// Libs
import React from 'react'
import styled from 'styled-components'

// Components
import './App.css'

// Phone list
import PHONES from './assets/phones/json'

function App() {
  const formatedPhones = formatPhones(PHONES)
  return (
    <FullContainer>
      <Title>Teléfonos disponibles</Title>
      <List>
        {formatedPhones.map((phone) => (
          <Phone>
            {phone.name}
            <Description>{phone.description}</Description>
          </Phone>
        ))}
      </List>
    </FullContainer>
  )
}

export default App

// //
// Aux functions
// //
function formatPhones(phones) {
  let formatedPhones = phones
  formatedPhones.map((p) => {
    return {
      name: p.n,
      description: p.d,
    }
  })
  return formatedPhones
}

// //
// Styles
// //
const FullContainer = styled.div`
  display: flex;
  position: absolute;
  top: 0px;
  left: 0px;
  width: 100%;
  align-items: center;
  flex-direction: column;
`

const Title = styled.div`
  font-size: 30px;
  font-weight: bold;
  width: 100%;
  text-align: center;
  color: blue;
`

const List = styled.div`
  display: flex;
  width: 60%;
  flex-direction: column;
  margin-top: 20px;
`

const Phone = styled.div`
  width: 100%;
  padding: 10px;
  margin-bottom: 10px;
  border: 1px solid black;
  cursor: pointer;

  &:hover {
    background: cyan;
  }
`

const Description = styled.div`
  font-size: 10px;
  color: grey;
`
