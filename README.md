# Sentisis FE developer test

## Instrucciones

Este proyecto ha sido creado con create-react-app con la intención de mostrar un listado
de teléfonos pero aún está sin acabar. Para dar por concluido el test hay que realizar
cada uno de los siguientes pasos:

1. Clonar el repositorio del proyecto e iniciarlo con el comando especificado en el package.json

2. El proyecto dará un error de compilación al iniciar, hay que arreglar dicho error para que
   la web se pueda levantar

3. Una vez funcionando la lista de teléfonos mostrará filas vacías, solucionar el problema y mostrar
   en cada fila el nombre y la descripción de cada teléfono

4. Las filas de los teléfonos son un poco feas, mejorar su visualización y además mostrar la imagen
   de cada teléfono
   **Bonus points:** Mostrar las filas pares con un color de fondo mas oscuro que las impares

5. Formatear la lista de teléfonos en el ciclo de vida idoneo del componente y guardar el resultado en
   su estado en lugar de llamarlo en la función de renderización.

6. Mejorar la calidad de código del proyecto siguiendo las recomendaciones de REACT y limpieza de código

7. Cada movil tendrá un contador que mide cuantas veces el usuario ha hecho click sobre el mismo, mostrándolo
   en la esquina superior derecha de cada fila.

## Agradecimientos

Muchas gracias por dedicar tu tiempo y esfuerzo en realizar este test que nos permite ver cómo trabajas
y comprobar que tus habilidades y conocimientos encajan con el perfil buscado.

Equipo Sentisis.
